package diplom.spend.view;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.text.DateFormatSymbols;

import diplom.spend.model.Item;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.XYChart;

public class BalanceStatisticsController {
	
	@FXML
    private BarChart<String, Integer> barChart;

    @FXML
    private CategoryAxis xAxis;

    private ObservableList<String> itemsNames = FXCollections.observableArrayList();

    @FXML
    private void initialize(List<Item> items) {
    	String[] strings =new String[items.size()];
    	
    	int i=0;
    	for (Item p : items) {
    		//strings;
    		strings[i]=p.getItemOfExp();
    		i++;
    	}
        itemsNames.addAll(Arrays.asList(strings));

        xAxis.setCategories(itemsNames);
    }
    
    public void setItemData(List<Item> items) {
    	XYChart.Series<String, Integer> series = new XYChart.Series<>();
        for (Item p : items) {
            series.getData().add(new XYChart.Data<>(p.getItemOfExp(), p.getBalance()));
        }

        barChart.getData().add(series);
    }

}
