package diplom.spend.view;

import java.util.Optional;

import diplom.spend.MainApp;
import diplom.spend.model.Item;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class ItemOverviewController {
	
	@FXML
    private TableView<Item> itemTable;
	
	@FXML
    private TableColumn<Item, String> itemColumn;
	@FXML
    private TableColumn<Item, Integer> costsColumn;
	
	@FXML
    private Label itemLabel;
	@FXML
    private Label costsLabel;
	@FXML
    private Label budgetLabel;
	@FXML
    private Label balanceLabel;

    private MainApp mainApp;

	public ItemOverviewController() {}
    
	@FXML
    private void initialize() {
		itemColumn.setCellValueFactory(cellData->cellData.getValue().itemOfExpProperty());
		costsColumn.setCellValueFactory(cellData->cellData.getValue().costsProperty().asObject());
		showItemDetails(null);

		itemTable.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue)->showItemDetails(newValue));
	}

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
		itemTable.setItems(mainApp.getItemData());
	}

	private void showItemDetails(Item item){
		if(item != null){
			itemLabel.setText(item.getItemOfExp());
			costsLabel.setText(Integer.toString(item.getCosts()));
			budgetLabel.setText(Integer.toString(item.getBudget()));
			balanceLabel.setText(Integer.toString(item.getBalance()));
		} else {
			itemLabel.setText("");
			costsLabel.setText("");
			budgetLabel.setText("");
			balanceLabel.setText("");
		}
	}

	@FXML
	private void handleDeleteItem() {
	    int selectedIndex = itemTable.getSelectionModel().getSelectedIndex();
	    String selectedItem = itemTable.getSelectionModel().getSelectedItem().getItemOfExp();
	    if(selectedIndex>=0){
	    	Alert alert = new Alert(AlertType.CONFIRMATION);
	    	alert.setTitle("Delete Dialog");
	    	alert.setHeaderText("Deleting " + selectedItem);
	    	alert.setContentText("Are you sure you want to delete " + selectedItem + " ?");

	    	Optional<ButtonType> result = alert.showAndWait();
	    	if (result.get() == ButtonType.OK){
	    	    // ... user chose OK
	    		itemTable.getItems().remove(selectedIndex);
	    	} else {
	    	    // ... user chose CANCEL or closed the dialog
	    		return;
	    	}
	    	
	    }else{
	    	Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("No Selection");
	        alert.setHeaderText("No Item Selected");
	        alert.setContentText("Please select an item in the table.");

	        alert.showAndWait();
	    }
	    
	}

	@FXML
	private void handleNewItem(){
		Item tempItem = new Item();
		boolean okClicked = mainApp.showItemEditDialog(tempItem);
		if(okClicked){
			mainApp.getItemData().add(tempItem);
		}
	}

	@FXML
	private void handleEditItem() {
	    Item selectedItem = itemTable.getSelectionModel().getSelectedItem();
	    if (selectedItem != null) {
	        boolean okClicked = mainApp.showItemEditDialog(selectedItem);
	        if (okClicked) {
	            showItemDetails(selectedItem);
	        }

	    } else {

	        Alert alert = new Alert(AlertType.WARNING);
	        alert.initOwner(mainApp.getPrimaryStage());
	        alert.setTitle("No Selection");
	        alert.setHeaderText("No Item Selected");
	        alert.setContentText("Please select an item in the table.");

	        alert.showAndWait();
	    }
	}
	
}
