package diplom.spend.view;

import diplom.spend.model.Item;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class ItemEditDialogController {
	
	@FXML
    private TextField itemField;
	@FXML
    private TextField costsField;
	@FXML
    private TextField budgetField;
	@FXML
    private Label balanceField;
	
	private Stage dialogStage;
    private Item item;
    private boolean okClicked = false;

    @FXML
    private void initialize() {
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

	public void setItem(Item item) {
		this.item = item;
		
		itemField.setText(item.getItemOfExp());
		costsField.setText(Integer.toString(item.getCosts()));
		budgetField.setText(Integer.toString(item.getBudget()));
		balanceField.setText(Integer.toString(item.getBalance()));
	}

	public boolean isOkClicked() {
        return okClicked;
    }

	@FXML
    private void handleOk() {
        if (isInputValid()) {
        	item.setItemOfExp(itemField.getText());
        	item.setCosts(Integer.parseInt(costsField.getText()));
        	item.setBudget(Integer.parseInt(budgetField.getText()));
        	item.setBalance(Integer.parseInt(balanceField.getText()));
        	
        	okClicked = true;
            dialogStage.close();
        }
     }

	@FXML
    private void handleCancel() {
        dialogStage.close();
    }

	private boolean isInputValid() {
		String errorMessage = "";
		
		if(itemField.getText()==null || itemField.getText().length()==0){
			errorMessage += "No valid item of expenditure!\n";
		}
		if(costsField.getText()==null || costsField.getText().length()==0){
			errorMessage += "No valid costs!\n";
		}else {
            try {
                Integer.parseInt(costsField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "No valid costs (must be an integer)!\n"; 
            }
		}
		if(budgetField.getText()==null || budgetField.getText().length()==0){
			errorMessage += "No valid budget!\n";
		}else {
            try {
                Integer.parseInt(budgetField.getText());
            } catch (NumberFormatException e) {
                errorMessage += "No valid budget (must be an integer)!\n"; 
            }
		}

		if (errorMessage.length() == 0) {
            return true;
        } else {
            Alert alert = new Alert(AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
	}
    
}
