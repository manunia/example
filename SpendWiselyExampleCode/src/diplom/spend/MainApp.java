package diplom.spend;

import java.io.File;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import diplom.spend.model.Item;
import diplom.spend.model.ItemListWrapper;
import diplom.spend.view.BalanceStatisticsController;
import diplom.spend.view.ItemEditDialogController;
import diplom.spend.view.ItemOverviewController;
import diplom.spend.view.RootLayoutController;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class MainApp extends Application {
	
	private Stage primaryStage;
    private BorderPane rootLayout;
    
    private ObservableList<Item> itemData = FXCollections.observableArrayList();
    
    public MainApp(){
    	itemData.add(new Item("Food"));
    	itemData.add(new Item("Public service"));
    	itemData.add(new Item("Health"));
    	itemData.add(new Item("Education"));
    	itemData.add(new Item("Beauty and care"));
    	itemData.add(new Item("Cars"));
    	itemData.add(new Item("Clothes"));
    	itemData.add(new Item("Entertainment"));
    	itemData.add(new Item("Unexpected"));
    }
    
    public ObservableList<Item> getItemData(){
    	return itemData;
    }

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Spend Wisely");

        this.primaryStage.getIcons().add(new Image("file:resources/images/coins.png"));
        
        initRootLayout();

        showItemOverview();
	}
	
	public void initRootLayout() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);

            RootLayoutController controller = loader.getController();
            controller.setMainApp(this);
            
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        File file = getItemFilePath();
        if (file != null) {
            loadItemDataFromFile(file);
        }
    }
	
	public void showItemOverview() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ItemOverview.fxml"));
            AnchorPane itemOverview = (AnchorPane) loader.load();

            rootLayout.setCenter(itemOverview);

            ItemOverviewController controller = loader.getController();
            controller.setMainApp(this);
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	public boolean showItemEditDialog(Item item){
		try {
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/ItemEditDialog.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();

	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Edit item of expenditure details");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        ItemEditDialogController controller = loader.getController();
	        controller.setDialogStage(dialogStage);
	        controller.setItem(item);

	        dialogStage.getIcons().add(new Image("file:resources/images/coins.png"));

	        dialogStage.showAndWait();

	        return controller.isOkClicked();
	    } catch (IOException e) {
	        e.printStackTrace();
	        return false;
	    }
	}

	public File getItemFilePath() {
	    Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
	    String filePath = prefs.get("filePath", null);
	    if (filePath != null) {
	        return new File(filePath);
	    } else {
	        return null;
	    }
	}

	public void setItemFilePath(File file) {
	    Preferences prefs = Preferences.userNodeForPackage(MainApp.class);
	    if (file != null) {
	        prefs.put("filePath", file.getPath());

	        primaryStage.setTitle("Spend Wisely - " + file.getName());
	    } else {
	        prefs.remove("filePath");

	        primaryStage.setTitle("Spend Wisely");
	    }
	}

	public void loadItemDataFromFile(File file) {
	    try {
	        JAXBContext context = JAXBContext.newInstance(ItemListWrapper.class);
	        Unmarshaller um = context.createUnmarshaller();

	        ItemListWrapper wrapper = (ItemListWrapper) um.unmarshal(file);

	        itemData.clear();
	        itemData.addAll(wrapper.getItems());

	        setItemFilePath(file);

	    } catch (Exception e) { // catches ANY exception
	        Alert alert = new Alert(AlertType.ERROR);
	        alert.setTitle("Error");
	        alert.setHeaderText("Could not load data");
	        alert.setContentText("Could not load data from file:\n" + file.getPath());

	        alert.showAndWait();
	    }
	}

	public void saveItemDataToFile(File file) {
	    try {
	        JAXBContext context = JAXBContext.newInstance(ItemListWrapper.class);
	        Marshaller m = context.createMarshaller();
	        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

	        ItemListWrapper wrapper = new ItemListWrapper();
	        wrapper.setItems(itemData);

	        m.marshal(wrapper, file);

	        setItemFilePath(file);
	    } catch (Exception e) { // catches ANY exception
	        Alert alert = new Alert(AlertType.ERROR);
	        alert.setTitle("Error");
	        alert.setHeaderText("Could not save data");
	        alert.setContentText("Could not save data to file:\n" + file.getPath());

	        alert.showAndWait();
	    }
	}

	public void showBalanceStatistics() {
	    try {
	        FXMLLoader loader = new FXMLLoader();
	        loader.setLocation(MainApp.class.getResource("view/BalanceStatistics.fxml"));
	        AnchorPane page = (AnchorPane) loader.load();
	        Stage dialogStage = new Stage();
	        dialogStage.setTitle("Balance Statistics");
	        dialogStage.initModality(Modality.WINDOW_MODAL);
	        dialogStage.initOwner(primaryStage);
	        Scene scene = new Scene(page);
	        dialogStage.setScene(scene);

	        BalanceStatisticsController controller = loader.getController();
	        controller.setItemData(itemData);

	        dialogStage.show();

	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	public Stage getPrimaryStage() {//�����, ������� ���������� ������� ����� javaFX
        return primaryStage;
    }

	public static void main(String[] args) {
		launch(args);
	}
}
