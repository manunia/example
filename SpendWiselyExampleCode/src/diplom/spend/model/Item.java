package diplom.spend.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Item {
	
	private final StringProperty itemOfExp;
	private final IntegerProperty costs;
	private final IntegerProperty budget;
	private final IntegerProperty balance;
	
	
	public Item() {
		this(null);
	}


	public Item(String itemOfExp) {
		this.itemOfExp = new SimpleStringProperty(itemOfExp);

		this.costs = new SimpleIntegerProperty(10000);
		this.budget = new SimpleIntegerProperty(20500);
		this.balance = new SimpleIntegerProperty(10500);
	}


	public String getItemOfExp() {
		return itemOfExp.get();
	}
	
	public void setItemOfExp(String itemOfExp) {
		this.itemOfExp.set(itemOfExp);
	}
	
	public StringProperty itemOfExpProperty(){
		return itemOfExp;
	}


	public int getCosts() {
		return costs.get();
	}
	
	public void setCosts(int costs){
		this.costs.set(costs);
	}
	
	public IntegerProperty costsProperty(){
		return costs;
	}
	
	public int getBudget() {
		return budget.get();
	}
	
	public void setBudget(int budget){
		this.budget.set(budget);
	}

	public IntegerProperty budgetProperty() {
		return budget;
	}
	
	public int getBalance() {
		return balance.get();
	}
	
	public void setBalance(int balance){
		balance = this.getBudget() - this.getCosts();
		this.balance.set(balance);
	}

	public IntegerProperty balanceProperty() {
		return balance;
	}

}
