package textRedactor;

import java.awt;
import java.io;
import javax.swing;

public class Action extends JFrame{
	/**
	 * поля для форматирования текста
	 */
	int style, size;
	
	Action() {
		final JFrame f = new JFrame("Text Redaсtor");//основной контейнер
		f.setSize(600, 600);

		MenuBar mbar = new MenuBar();//проинициализируем менюбар
		f.setMenuBar(mbar);//добавим менюбар в основной контейнер
		
		final TextArea text = new TextArea("Enter text", 80,40);//создадим большое текстовое поле для нашего редактора
		/**
		 * @param clipbd - buffer
		 */
		Clipboard clipbd = getToolkit().getSystemClipboard();
		
		f.add("Center", text);
		text.setEditable(true);
		
		//панель, на которой будут располагаться кнопки для форматирования текста
		final JPanel p1 = new JPanel();

		//массив для выпадающего списка
		String[] items = {"Times New Roman", "Verdana", "Helvetica", "Courier", "Symbol"};
		//проинициализируем кнопки для форматирования
		JButton b1 = new JButton("I");//курсив
		JButton b2 = new JButton("B");//жирный
		JButton b3 = new JButton("P");//плановый
		final JComboBox box = new JComboBox(items);
		box.addItem(items);
		ButtonGroup g = new ButtonGroup();//сгруппируем кнопки
		g.add(b1);
		g.add(b2);
		g.add(b3);
		//добавляем элементы на панель
		p1.add(box);
		p1.add(b1);
		p1.add(b2);
		p1.add(b3);
		f.add("South", p1);
		
		//установим элементам слушателей событий
		b1.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
				text.setFont(new Font("italica", Font.ITALIC, 14));
				style = Font.ITALIC;
			}
		});
		
		b2.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
				text.setFont(new Font("bold", Font.BOLD, 14));
				style = Font.BOLD;
			}
		});
		
		b3.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
				text.setFont(new Font("bold+italica", Font.PLAIN, 14));
				style = Font.PLAIN;
			}
		});

		box.addItemListener(new ItemListener(){
			public void itemStateChanged (ItemEvent e) {
				int index = box.getSelectedIndex();
				if(e.getSource()==box) {
					if(index==0) {
						text.setFont(new Font("TimesRoman", style, 14));
					}
					if(index==1) {
						text.setFont(new Font("Verdana", style, 14));
					}
					if(index==2) {
						text.setFont(new Font("Helvetica", style, 14));
					}
					if(index==3) {
						text.setFont(new Font("Courier", style, 14));
					}
					if(index==4) {
						text.setFont(new Font("Symbol", style, 14));
					}
				}	
			}
		});
		//создадим элементы меню и добавим их в менюбар
		Menu file = new Menu("File");
		MenuItem mn_newitem = new MenuItem("New....");
		file.add(mn_newitem);

		mn_newitem.addActionListener(new ActionListener(){
			public void actionPerformed (ActionEvent e) {
				text.setText("");
			}
		});

		MenuItem mn_openitem = new MenuItem("Open...");
		file.add(mn_openitem);
		MenuItem mn_savenitem = new MenuItem("Save As...");
		file.add(mn_savenitem);
		file.add(new MenuItem("-"));
		MenuItem mn_quititem = new MenuItem("Quit...");
		file.add(mn_quititem);
		
		mn_quititem.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				System.exit(0);
			}
		});
		/**
		 * @see mn_openitem Open menu
		 */
		mn_openitem.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				if( fc.showOpenDialog(fc) == JFileChooser.APPROVE_OPTION){
					File ff = fc.getSelectedFile();    
			        BufferedReader in = null;
			        try {
			        	in = new BufferedReader(new FileReader(ff));
			        	for(;;) {
			        		String str = in.readLine();
			                if ( str == null )
			                    break;
			                text.append(str+"\n");
			        	}
			        } catch (IOException ex) {
			        	ex.printStackTrace();
			        } finally {
			        	if ( in != null )
			        		try {
			        			in.close();
			        		} catch ( IOException ex ) {
			        			
			        		}
			        }
				}
			}	
		});
		/**
		 * @see mn_saveitem Save menu
		 */
		mn_savenitem.addActionListener(new ActionListener() {
			
			public void actionPerformed (ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				if( fc.showSaveDialog(fc) == JFileChooser.APPROVE_OPTION){
					File ff = new File(fc.getSelectedFile().getAbsolutePath());
					
					try {
						if(!ff.exists()){
							ff.createNewFile();
						}
						PrintWriter out = new PrintWriter(ff.getAbsoluteFile());
						try{
							out.print(text);
						} finally {
							out.close();
						}
					} catch (IOException e1) {
						throw new RuntimeException(e1);
					}

				}
			}
		});
		
		
		mbar.add(file);
		
		final Menu edit = new Menu("Edit");
		
		MenuItem mn_cutitem = edit.add(new MenuItem("Cut"));
		mn_cutitem.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				text.replaceText("", text.getSelectionStart(), text.getSelectionEnd());
			}
		});
		

		MenuItem mn_copyitem = edit.add(new MenuItem("Copy"));

		MenuItem mn_pasteitem = edit.add(new MenuItem("Paste"));
		/**
		 * @see mn_copyitem Copy menu
		 */
		mn_copyitem.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				
				String selection = text.getSelectedText();
				if(selection==null)
					return;
				StringSelection clipString = new StringSelection(selection);
				clipbd.setContents(clipString, clipString);
			}
		});
		/**
		 * @see mn_pasteitem Paste menu
		 */
		mn_pasteitem.addActionListener(new ActionListener() {
			public void actionPerformed (ActionEvent e) {
				
				Transferable clipData = clipbd.getContents(mn_copyitem);
				try{
					String clipString = (String)clipData.getTransferData(DataFlavor.stringFlavor);
					text.replaceRange(clipString, text.getSelectionStart(), text.getSelectionEnd());
				} catch (Exception ex) {
					System.err.println("Not String");
				}
			}
		});
		 
		mbar.add(edit);
		
		f.setVisible(true);
	}

}
